(function (global) {
    var imageViewModel,
        app = global.app = global.app || {};
    
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        
       /* var html='';
        for (var key in evt) {
            html += key + '  ' + evt[key] + '<br />';
        }*/
        $('#output').html ( evt.x.client );
        
        return {
          x: evt.x.client - rect.left,
          y: evt.y.client - rect.top
        };
        /*x: evt.clientX - rect.left,
          y: evt.clientY - rect.top*/
    }
    
    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }


    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }
    

     imageViewModel = kendo.data.ObservableObject.extend({
    	onViewInit: function () {console.log ('init')},
		onViewShow: function () {app.imageImport.viewModel.run(); app.imageImport.viewModel.solution.init()},
         originalImage: null,
        _pictureSource: null,
        _destinationType: null,
         viewMode: {
             allowCapture: true,
             imageLoaded: false,
             allowCrop: false,
             imageReady: false
         },
         viewMode_allowCapture: true,
         viewMode_imageLoaded: false,
         viewMode_allowCrop: false,
         viewMode_imageReady: false,
         viewMode_setIncorrectAnswers: false,
         viewMode_setCorrectAnswer: false,
        run: function(){
            var that=this;
    	    that._pictureSource = navigator.camera.PictureSourceType;
    	    that._destinationType = navigator.camera.DestinationType;
        },
        
        _capturePhoto: function() {
            var that = this;
            
            // Take picture using device camera and retrieve image as base64-encoded string.
            navigator.camera.getPicture(function(){
                that._onPhotoDataSuccess.apply(that,arguments);
            },function(){
                that._onFail.apply(that,arguments);
            },{
                quality: 50,
                destinationType: that._destinationType.DATA_URL
            });
        },
     
        _getPhotoFromLibrary: function() {
            var that= this;
            // On Android devices, pictureSource.PHOTOLIBRARY and
            // pictureSource.SAVEDPHOTOALBUM display the same photo album.
            that._getPhoto(that._pictureSource.PHOTOLIBRARY);         
        },
        filmStrip:[],
        _getPhotoFromAlbum: function() {
            app.imageImport.viewModel.set('viewMode_imageLoaded', false);
             app.imageImport.viewModel.set('viewMode_allowCrop', false);
            var dataSource = new kendo.data.DataSource({
              transport: {
                read: {
                  url: "cfg/sampleImages.json",
                  dataType: "json"
                }
              }
            });
            dataSource.fetch(function() {
                app.imageImport.viewModel.set('filmStrip', this.data());
            });
        },
        
        _getPhoto: function(source) {
            var that = this;
            // Retrieve image file location from specified source.
            navigator.camera.getPicture(function(){
                that._onPhotoURISuccess.apply(that,arguments);
            }, function(){
                that._onFail.apply(that,arguments);
            }, {
                quality: 50,
                destinationType: that._destinationType.FILE_URI,
                sourceType: source
            });
        },
         selectFromFilmStrip: function(e) {
             var that = this;
             that.set('originalImage', e.item.find('img').attr('src'))
             that.set('filmStrip', []);
             app.imageImport.viewModel.set('viewMode_imageLoaded', true);
             app.imageImport.viewModel.set('viewMode_allowCrop', true);
         },
        
        _onPhotoDataSuccess: function(imageData) {
            //var smallImage = document.getElementById('originalImage');
            //smallImage.style.display = 'block';
        
            // Show the captured photo.
            //smallImage.src = "data:image/jpeg;base64," + imageData;
            app.imageImport.viewModel.set('originalImage', "data:image/jpeg;base64," + imageData);
            //app.newStory.viewModel.set("image.original", "data:image/jpeg;base64," + imageData);
            app.imageImport.viewModel.set('viewMode_imageLoaded', true);
            app.imageImport.viewModel.set('viewMode_allowCrop', true);
        },
        
        _onPhotoURISuccess: function(imageURI) {
            
            //var smallImage = document.getElementById('originalImage');
            //smallImage.style.display = 'block';
             
            // Show the captured photo.
            //smallImage.src = imageURI;
            app.imageImport.viewModel.set('originalImage', imageURI);
            //app.newStory.viewModel.set("image.original", imageURI);
            app.imageImport.viewModel.set('viewMode_imageLoaded', true);
            app.imageImport.viewModel.set('viewMode_allowCrop', true);
        },
        
        _onFail: function(message) {
            app.imageImport.viewModel.set('imageReady', false);
            alert(message);
        },
        
		createCrop: function () {
			$('#originalImage').cropper({
				aspectRatio: 4 / 3,
				crop: function (data) {
					// Output the result data for cropping image.
				},
				zoomable: false,
				mouseWheelZoom: false,
				strict: true
			});
		},
        finaliseCrop: function() {
            var result = $('#originalImage').cropper("getCroppedCanvas", {
                width: app.settings.canvasDimensions().x,
                height: app.settings.canvasDimensions().y                
            });
            $('#croppedImage').html(result);
            
            var canvas = document.getElementById('croppedImage').getElementsByTagName('canvas')[0];
            console.log (canvas.toDataURL);
            //var uri = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAIZAs0DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0CiiigAooooAKKKKACiiigAoopKAFopKKAFopKZJPFEMyyog9WYCgCSiqEutadF967j/4D838qpyeJ7FDhFmk91TigDbormpfFRx+5s2+rtiq0niXUGyEjt4x64JI/WgDraY00SyrE0iiR+VUnk/hXFy6zqbg5uiM9lUCs5zMzmR5Xd26szHNF0B6LLPDAMzSpGP9pgKoza7psPW7Rj6LzXD4P8Rzn8aTeR0OKVwOuk8U2Yz5cU7n/dAB/Wqsnimb/lnZqPTc9c3vbqSQaXccdTRcLGzL4i1J/umKP/dXP86qT6jqE3Ml1N9EO0fpVEH1b9KMjPLj8c/4UXCxI5ZzmQs7HuxJpCO/lj8BUYycncfwP/1qUZ7k/wCfwpXGKR7frRgAU0Fs5oLuD2oAfgDHIowOmRVaa+WEfvCM9gOTWbc6pI5IU7B6DrTSC5qXN1FbjDMM9gOTWZcak7ZC/IPbk1ntKznjv+ZpFUk4P5CqsIe0rEnHU9+9CqT9fSpYbZnOAM+wrRgsVXBf8hQ3YLFGG2ZzgA/QVowWCoAXP4CrShUGFAA9qC3FS5DFUKgwoAFIzU0vTcE1ICl/Sm8mnBKeEpgRYoxUpQYo2UARcinhvWnbKQpQA4NTjhhgjIqHBFOD+tICKaySQfJx7Gs6e0eM9K2Q1BwwwQCKadgOdIKnvTlmIIznI79DWrPZK4+Tj2NZ01syHGPzq07iLtvqOMCRd3uOD+VX4p4pRmPB+hrmyGU96ek7KwPcd+9FgudNuI6A0bu9ZFtqR6SDePbrWhFPFOP3b/getTYLlkgdSp/EUqSPEcxO8f8Aukio1OBgk/nRuB4yfzpDFuZZblgZ5WlIGBvOcVXeAH7iD8Of6VPwaTHOMUBcrNAw4zj/AHkFQtGwY42t9MfyrQPpjmkOT1oHcseFwy65bFlI+91XH8Jrv685T5WDL8pHdeKuxapex/cu5fox3fzpoTO5orjP7d1ExshnVgRjO0Aj8RVmx8RT26BLiNp1H8Wfm/8Ar0xHV0Vjw+I7B/8AWeZF/vJn+VXYdSsp8CO6iJPbdg/lQBbopAwPQg0UALRSUUALRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRUT3MEf3541+rgVSm17TYSQbpGI67OaANKkrCl8V2Y4iill/AAVVl8VyniGzUe7Pn+VAXOnori5fEmpvkK0cf+6mapTapey5E15MQewOBRdAd+8scf35FX6nFVJdX0+E4e7iz6Bs/wAq4EzZGGaRvqaEkRegYfSlcR2U3ibT4+E82U/7Cf41WbxWp/1djIf99wtcwZYyMsHIpd8Zx8mP+BUXHY3pPE96W/d28Cr/ALTZNUZdf1R2/wBftHoir/Os/eCOAAPc0bs/3cfWi7HYnl1G8nBElzcEHqN+B+hqs3zfeDMfU/8A66Xf6AU0kZ4BouGgDgcLj8B/jSb2B+7/AC/xpMj/ACKM/T8qkBwY9Shz9RS+YR/Afz/+vSDj0/Klzjq36UwHYbOdh/OkJZRkqRSBl7k/980hPOQeKYx24nkg/lRv9j+VM3ep/Sgyeh/SkA7eD/C35Ubj2U/rSebxyV/KkMo6Arj6UCHAs3rj8aX5u+cfQ/4VG0ygYJH5UCRD905NAEhYA4+b8j/hSllxyxH4H/Cqs1zDEMZLN6DrWXc6jI2QG2j0Xr+JppAa095BEP8AWFm/ujrWZc6pI/yodg9Acn86zmkZuBx7CmhSTj9BVJCHtMzdM8/maRVJ/wAKljgLcfoKuRWyr978hTAqxW7OcAfgK0IbNVHz/kKkQhRhRgU8NUtsCRAqjCgCn7qiBzT1BJqRji3pQATT1jqVY6AIRHUix1KFA60E9gOaAG7AOtNLAcDrTtrN1B/OnBCOin9KhyHYgIz1bH0NKBt/iBqxtP8AdNLsP90/mKm47EKsDTtgIpxjz/Cabhl7GrUhWGGOoylWQwPWlKAjiqEU8FaN3rU7R1E0dACbqa+GGGAIpCCKbu9aAK81orZ2/kaoS2zKen51qlqa+1hhhmqTEYpVlNPSdlPOTjv0NXpbdT93n2NU5ICD/Q1V7gXbfUWXAb94PfrWjDPHOuUIyOoJwa5zaR359DSrKynnPH4GlYDpiCOdv60mSegP51kW+osvBw4HY8GtKG5im+4T9D1/nSaGTAnrg/lTsgj/AOtTRjPT9f8A69JhOpX8jUgPz7/pS5/zimEJnO39aUMvZT/31QA/6Z/Klz7H8qj34wcGkMmOz/rQBIeemfyNLx3GfrUYlB/v/rQXHXD5+hoAkVlVsoSh9jV2LV76IgpeSH2f5h+tZwbd0B/HNO2k/wAJH4U7isjci8S3aj95HFJj0BBNXY/FEB/1ttKn+6Q1cvyPXP0oJI9c0XCx2cev6bJj/SNh9GUjFXIru2m/1VxE/wDuuDXn/mD+9SFlPXb+Jp3A9Horz2C8lh/1FzJH7LJgflV6LXdQjXi6R8dnANAHa0lcrD4puF/10UMn+6Sv+NWU8WQZ/e2sq+6kNQB0VFZUPiLTJnC/aNjH++pH69K0IbmCf/UzRyf7jA/yoAlopKKAFopKKAFooooAKKKKACiikoA8fF5cL/BGfqCf60v2+cchIwfYYphFJinYRKNRuM8ohpTqc+MeVH+FQkUmM0WQE/8Aak46RJ+dIdTm5zCn51Dto20WQEv9pS/88R/31R/acn/PAZ/3/wD61Q4oK0WC5N/acmf9Qf8Av5/9aj+05O9v/wCP/wD1qhxRiiwXJ/7Tc4zA34PS/wBqHPNu2P8AezVbHNLijlQyx/aa4x9nf86Q6khH/Hu/51X2ijbRyoRZ/tOMdLd/zpRqcPeCT/P41V28UbRRyoZaXU4AeYJPy/8Ar0v9qw54ilH/AAEf41U2CjaPSiwXLf8Aa0WeY5P++R/jR/akJ/5Zyf8AfI/xqpsGaNg9KLAWv7Ug7xy4+lKdStscLL+VVdgo2CjlQFkanb9lkz/u0o1S36ES/wDfNVdgpNoHajlC5b/tS39JD/wD/wCvVe41bLERB1QjuMEmmY46VFMgZehPsKLBcryTvISOg9BTAMnoT7CrKWxP3gVHoBUv2fb93A+tMRWSEt1GB6CrCIiDt9BTvJf++PypRAf7w/KgY5XA4Ap4bNMERHcflTwCvpSsBIoJqZFzUAdh2FPFw46KtKwFtI/Wp0T2rPF5MPuon5H/ABqzaXUsrsrhRgZAUUNWAuBQvWlLegpACeoapFjX/nmfyrNyKsMCM3fFPWI9m/SpFjX/AJ5n/vmnhF/55n/vms7jGCNv736U4RN/e/SpAif88j/3zS7E/wCeR/75pDI/Kb+9+lJ5T/3v0qbYn/PI/wDfFHlx/wDPM/8AfBoAgMTf3h+VNMbf3h+VWDGg/wCWZ/74NNMcf9w/98mgRXaJvUflTNrrz1qyUj/un/vk1GUj/un8jTTAjDZ4IpGQEcUpSP0P5GmH5TwSfwNaKQrEbx1XdKsyzBYmbHIBPNZxv2PVFP0q1qSOYEVESRSm63D7n61G0uf4cfjTsFxSxprMGHNNLZphBPcYp2AHiVh61XeEjpyPQ1Ptb1FLhu5FMClt544PoacsrKRnPH51ZMW4fNt/OozAwHUEe9AFm1v/";
             //window.localStorage.setItem( "image-base64", canvas.toDataURL() );
               //app.fileApp._writeTextToFile('img', image);
            var uri = canvas.toDataURL();
            var fileName = 'canvas',
                folderName = 'folder';
            
            fileName = "canvas.png",
    		//uri = encodeURI("http://www.telerik.com/sfimages/default-source/logos/app_builder.png"),
    		folderName = "test";
            app.fileApp.saveImg(uri, fileName, folderName);
            app.imageImport.viewModel.set('viewMode_imageReady', true);
            var that = this;
            that.pinBoard.shapeToolbar.init();
            //app.newStory.viewModel.set("image.cropped", result.toDataURL);
        },  
        
		_repositionBaseImage: function () {
			var $baseImageWrap = $('#originalImage'),
				$baseImage = $('#originalImage img'),
				orientation = '',
				width = $baseImage.width(),
				height = $baseImage.height();

			if (width < height) {
				orientation = 'portrait';
			}
			$baseImageWrap.attr('data-orientation', orientation);
			console.log($baseImage.width(), $baseImage.height());
			return orientation;
		},
         
         pinBoard: {
               shapeToolbar: {
                    init: function() {
                        $("#toolbar-shapes ul").kendoMobileButtonGroup({
                            select: function(e) {
                                console.log (this.current().data('icon'));
                                app.imageImport.viewModel.set("pinBoard.shapeToolbar.selectedItem", this.current().data('icon'))
                            }
                        });
                        
                        var pinWrap = document.getElementById('canvas-wrap'),
                            overlay = document.getElementById('touchOverlay'),
                            canvas = pinWrap.getElementsByTagName("canvas")[0];
                        pinWrap.setAttribute('style','width:' + app.settings.canvasDimensions().x + 'px; height:' + app.settings.canvasDimensions().y + 'px');
                        $(overlay).kendoTouch({
                            tap: function (e) {
                                var initialObj = e.touch.initialTouch;
                                
                                if (initialObj.nodeName === "I") {
                                    initialObj.remove();
                                } else {                               
                                    var pos = getMousePos(overlay, e.touch),
                                        pin = document.createElement('i');
                                    pin.setAttribute('style', 'left:'+pos.x+'px; top:'+pos.y+'px;');
                                    pin.setAttribute('data-icon', app.imageImport.viewModel.pinBoard.shapeToolbar.selectedItem);
                                    overlay.appendChild(pin);
                                 }
                            }
                        });
                    },
                    selectedItem: null
                },
             pins: []
         },
         progressValue:10,
         progressBar: {
             progressValue:40
         },
         soundRecorder: {
             record:function() {
                console.log ('record');
                var that = this;
                app.storyRecorder._capureAudio.apply(that, arguments)  
            },
            play:function() {
                console.log ('play');
                var that = this;
               app.storyRecorder._play.apply(that, arguments)
            },
            pause: function() {
               var that = this;
               app.storyRecorder._pause.apply(that, arguments)
            },
            stop: function() {
                var that = this;
               app.storyRecorder._stop.apply(that, arguments)
            }
        },
        
        solution: {
            init: function() {
                var that = this;
                $("#numericEntry").kendoTouch({
                 tap: function (e) {
                        var initialObj = e.touch.initialTouch;
                        
                        if (initialObj.nodeName === "LI") {
                            that.enter (initialObj);
                        }
                    }
                });
            },
            polarity: function() {
                var num = app.imageImport.viewModel.correctAnswer;
                if (num.charAt(0) === "-") {
                    num = num.replace('-','');
                } else {
                    num = "-" + num;
                }
                
                app.imageImport.viewModel.set('correctAnswer',  num);
            },
            clear: function() {
                app.imageImport.viewModel.set('correctAnswer',  '0');
            },
            enter: function(obj) {
                var fn = obj.getAttribute('data-fn'),
                    that = this;
                if (fn === 'enter') {
                  var prev = app.imageImport.viewModel.correctAnswer,
                    newNum;
                    if ((obj.innerHTML === '.' && prev.indexOf('.') === -1) || obj.innerHTML !== '.') {
                        newNum = prev + obj.innerHTML;    
                        app.imageImport.viewModel.set('correctAnswer',  newNum);
                    }
                } else {
                    that[fn]();
                }
            },
            createRandomIncorrectChoices: function() {
                app.imageImport.viewModel.set('viewMode_setIncorrectAnswers', true);
                app.imageImport.viewModel.set('viewMode_setCorrectAnswer', false);
                
                var total = 32,
                total_negInt = 0, total_posInt = 0, total_negFloat = 0, total_posFloat = 0,
                base = app.imageImport.viewModel.correctAnswer,
                array = [];
                
                if (base.charAt(0) === "-" && base.indexOf('.') === -1) {//negative integer
                    total_negInt = total_posInt = .5;
                }
                if (base.charAt(0) === "-" && base.indexOf('.') !== -1) {//negative float
                    total_negInt = total_posInt = total_negFloat = total_posFloat = .25;
                }
                if (base.charAt(0) !== "-" && base.indexOf('.') !== -1) {//positive float
                    total_posInt = total_posFloat = 0.5;
                }
                if (base.charAt(0) !== "-" && base.indexOf('.') === -1) {//positive integer
                    total_posInt = 1;
                }
                console.log (total_negInt, total_posInt, total_negFloat, total_posFloat);
                
                for (var i = 0; i < total * total_posInt; i++) {
                    array.push(getRandomInt(0, base*1.5));
                }
                for (var i = 0; i < total * total_negInt; i++) {
                    array.push(-1*getRandomInt(0, base*1.5));
                }
                for (var i = 0; i < total * total_posFloat; i++) {
                    array.push(getRandomArbitrary(0, base*1.5).toFixed(2));
                }
                for (var i = 0; i < total * total_negFloat; i++) {
                    array.push(-1*getRandomArbitrary(0, base*1.5).toFixed(2));
                }
                var shuffledArray = shuffle(array);
                app.imageImport.viewModel.set('incorrectAnswers_random',  shuffledArray);
                
                var incorrectList = $('#incorrectAnswers_random');
                incorrectList.kendoTouch({
                    tap: function(e) {
                        var initialObj = e.touch.initialTouch,
                            vm = app.imageImport.viewModel,
                            theClass = "k-state-selected";
                        
                        if (initialObj.nodeName === "LI") {
                            var $node = $(initialObj);
                            if (vm.incorrectAnswers_selected.length < 3) {
                               $node.toggleClass(theClass);
                            } else {
                               if ($node.hasClass(theClass)) {
                                   $node.removeClass(theClass);
                               }
                            }
                            var selection = [];
                            incorrectList.find('.'+theClass).each(function(i,v) {
                                selection.push(v.innerHTML);
                            });
                            vm.set('incorrectAnswers_selected', selection);
                        }
                        
                    }
                })
            }
        },
        correctAnswer:'0',
        incorrectAnswers_selected:[],
        incorrectAnswers_random:[],
        Story: {
            data: {
                author: null,
                title:null,
                audio:null,
                image:{original:'', cropped:''},
                pins:null,
           },
            reset: function() {
                var that = this;
                that.data.set('author', null);
                that.data.set('title', null);
                that.data.set('audio', null);
                that.data.set('image', {original:'', cropped:''});
            },
            save: function() {}
        }
    });
     
     app.imageImport = {
        viewModel: new imageViewModel()
    };
})(window);
   
      /*



  _capturePhotoEdit: function() {
            var that = this;
            // Take picture using device camera, allow edit, and retrieve image as base64-encoded string. 
            // The allowEdit property has no effect on Android devices.
            navigator.camera.getPicture(function(){
                that._onPhotoDataSuccess.apply(that,arguments);
            }, function(){
                that._onFail.apply(that,arguments);
            }, {
                quality: 20, allowEdit: true,
                destinationType: that._destinationType.DATA_URL
            });
        },*/
        
  /*   captureAudio:function() {
             
            var that = this;
             that.set('title', 'recording');
            navigator.device.capture.captureAudio(function() {
                that._captureSuccess.apply(that, arguments);
            }, function() { 
                that._captureError.apply(that, arguments);
            }, {limit:1});
        },
       
        _captureImage:function() {
            var that = this;
            navigator.device.capture.captureImage(function() {
                that._captureSuccess.apply(that, arguments);
            }, function() { 
                that._captureError.apply(that, arguments);
            }, {limit:1});
        },
        
        _captureSuccess:function(capturedFiles) {
            var i,
            media = document.getElementById("media");
            media.innerHTML = "";
            for (i=0;i < capturedFiles.length;i+=1) {
                media.innerHTML+='<p>Capture taken! Its path is: ' + capturedFiles[i].fullPath + '</p>'
            }
        },
        
        _captureError:function(error) {
            if (window.navigator.simulator === true) {
                alert(error);
            }
            else {
                var media = document.getElementById("media");
                media.innerHTML = "An error occured! Code:" + error.code;
            }
        }, */