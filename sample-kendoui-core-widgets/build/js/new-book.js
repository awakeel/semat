(function (global) {
    var app = global.app = global.app || {};
    
    function NewStory() {};
    
    NewStory.prototype = {
        id: null,
        author: null,
        title:null,
        audio:null,
        image:null,
        pins:null,
        solution: {
            correctAnswer:null,
            incorrectAnswers: []
        }
    }

    app.newStory = {
        model: {
            id: null,
            author: null,
            title:null,
            audio:null,
            image:null,
            pins:null,
            solution: {
                correctAnswer:null,
                incorrectAnswers: []
            }
        },
        reset: function() {
            var that = this;
            that.model = {
                author: null,
                title:null,
                audio:null,
                image: null,
                pins:null,
                solution: {
                    correctAnswer:null,
                    incorrectAnswers: []
                }
            }
        }
    };
})(window);