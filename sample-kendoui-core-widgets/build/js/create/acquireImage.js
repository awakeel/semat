(function (global) {
    var acquireImageViewModel,
        app = global.app = global.app || {};
    
    acquireImageViewModel = kendo.data.ObservableObject.extend({
        progressValue: 20,
    	onViewInit: function () {console.log ('init')},
		onViewShow: function () {
            var that=this;
    	    that._pictureSource = navigator.camera.PictureSourceType;
    	    that._destinationType = navigator.camera.DestinationType;
        },
         originalImage: null,
        _pictureSource: null,
        _destinationType: null,
    
         viewMode_allowCapture: true,
         viewMode_imageLoaded: false,
         viewMode_allowCrop: false,
         viewMode_imageReady: false,
        
        _capturePhoto: function() {
            var that = this;
            navigator.camera.getPicture(function(){
                that._onPhotoDataSuccess.apply(that,arguments);
            },function(){
                that._onFail.apply(that,arguments);
            },{
                quality: 50,
                destinationType: that._destinationType.DATA_URL
            });
        },
     
        _getPhotoFromLibrary: function() {
            var that= this;
            that._getPhoto(that._pictureSource.PHOTOLIBRARY);         
        },
        filmStrip:[],
        _getPhotoFromAlbum: function() {
            app.acquireImage.viewModel.set('viewMode_imageLoaded', false);
             app.acquireImage.viewModel.set('viewMode_allowCrop', false);
            var dataSource = new kendo.data.DataSource({
              transport: {
                read: {
                  url: "cfg/sampleImages.json",
                  dataType: "json"
                }
              }
            });
            dataSource.fetch(function() {
                app.acquireImage.viewModel.set('filmStrip', this.data());
            });
        },
        
        _getPhoto: function(source) {
            var that = this;
            // Retrieve image file location from specified source.
            navigator.camera.getPicture(function(){
                that._onPhotoURISuccess.apply(that,arguments);
            }, function(){
                that._onFail.apply(that,arguments);
            }, {
                quality: 50,
                destinationType: that._destinationType.FILE_URI,
                sourceType: source
            });
        },
         selectFromFilmStrip: function(e) {
             var that = this;
             that.set('originalImage', e.item.find('img').attr('src'))
             that.set('filmStrip', []);
             app.acquireImage.viewModel.set('viewMode_imageLoaded', true);
             app.acquireImage.viewModel.set('viewMode_allowCrop', true);
         },
        
        _onPhotoDataSuccess: function(imageData) {
            app.acquireImage.viewModel.set('originalImage', "data:image/jpeg;base64," + imageData);
            app.acquireImage.viewModel.set('viewMode_imageLoaded', true);
            app.acquireImage.viewModel.set('viewMode_allowCrop', true);
        },
        
        _onPhotoURISuccess: function(imageURI) {
            
            app.acquireImage.viewModel.set('originalImage', imageURI);
            
            app.acquireImage.viewModel.set('viewMode_imageLoaded', true);
            app.acquireImage.viewModel.set('viewMode_allowCrop', true);
        },
        
        _onFail: function(message) {
            app.acquireImage.viewModel.set('imageReady', false);
            alert(message);
        },
        
		createCrop: function () {
			$('#originalImage').cropper({
				aspectRatio: 4 / 3,
				crop: function (data) {
					// Output the result data for cropping image.
				},
				zoomable: false,
				mouseWheelZoom: false,
				strict: true
			});
		},
        finaliseCrop: function() {
            var result = $('#originalImage').cropper("getCroppedCanvas", {
                width: app.settings.canvasDimensions().x,
                height: app.settings.canvasDimensions().y                
            });
            $('#croppedImage').html(result);
            
            var canvas = document.getElementById('croppedImage').getElementsByTagName('canvas')[0];
            var uri = canvas.toDataURL();
            var fileName = 'canvas',
                folderName = 'folder';
            
            fileName = "canvas.png",
    		folderName = "test";
            app.fileApp.saveImg(uri, fileName, folderName);
            app.system.navigate('/build/views/investigateImage.html');
            //app.acquireImage.viewModel.set('viewMode_imageReady', true);
           
        }
         });
    app.acquireImage = {
        viewModel: new acquireImageViewModel()
    };
})(window);