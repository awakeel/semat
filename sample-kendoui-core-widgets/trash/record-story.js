(function (global) {
    var recordStoryViewModel,
        app = global.app = global.app || {};
  

    recordStoryViewModel = kendo.data.ObservableObject.extend({
        record:function() {
            console.log ('record');
            var that = this;
            app.storyRecorder._capureAudio.apply(that, arguments)  
        },
        play:function() {
            console.log ('play');
            var that = this;
           app.storyRecorder._play.apply(that, arguments)
        },
        pause: function() {
           var that = this;
           app.storyRecorder._pause.apply(that, arguments)
        },
        stop: function() {
            var that = this;
           app.storyRecorder._stop.apply(that, arguments)
        }
    });

    app.recordStory = {
        viewModel: new recordStoryViewModel()
    };
})(window);