function id(element) {
return document.getElementById(element);
}
 document.addEventListener("deviceready", function () {
    navigator.splashscreen.hide();
   //app.imageImport.viewModel.run();
   captureApp = new captureApp();
   var app = new kendo.mobile.Application(document.body );
}, false);


function captureAppRun(){
    captureApp.run()
}

function cameraApp(){}

cameraApp.prototype={
    _pictureSource: null,
    
    _destinationType: null,
    
    run: function(){
        
        var that=this;
	    that._pictureSource = navigator.camera.PictureSourceType;
	    that._destinationType = navigator.camera.DestinationType;
	    id("capturePhotoButton").addEventListener("click", function(){
            that._capturePhoto.apply(that,arguments);
        });
	    id("capturePhotoEditButton").addEventListener("click", function(){
            that._capturePhotoEdit.apply(that,arguments)
        });
	    id("getPhotoFromLibraryButton").addEventListener("click", function(){
            that._getPhotoFromLibrary.apply(that,arguments)
        });
	    id("getPhotoFromAlbumButton").addEventListener("click", function(){
            that._getPhotoFromAlbum.apply(that,arguments);
        });
    },
    
    _capturePhoto: function() {
        var that = this;
        
        // Take picture using device camera and retrieve image as base64-encoded string.
        navigator.camera.getPicture(function(){
            that._onPhotoDataSuccess.apply(that,arguments);
        },function(){
            that._onFail.apply(that,arguments);
        },{
            quality: 50,
            destinationType: that._destinationType.DATA_URL
        });
    },
    
    _capturePhotoEdit: function() {
        var that = this;
        // Take picture using device camera, allow edit, and retrieve image as base64-encoded string. 
        // The allowEdit property has no effect on Android devices.
        navigator.camera.getPicture(function(){
            that._onPhotoDataSuccess.apply(that,arguments);
        }, function(){
            that._onFail.apply(that,arguments);
        }, {
            quality: 20, allowEdit: true,
            destinationType: cameraApp._destinationType.DATA_URL
        });
    },
    
    _getPhotoFromLibrary: function() {
        var that= this;
        // On Android devices, pictureSource.PHOTOLIBRARY and
        // pictureSource.SAVEDPHOTOALBUM display the same photo album.
        that._getPhoto(that._pictureSource.PHOTOLIBRARY);         
    },
    
    _getPhotoFromAlbum: function() {
        var that= this;
        // On Android devices, pictureSource.PHOTOLIBRARY and
        // pictureSource.SAVEDPHOTOALBUM display the same photo album.
        that._getPhoto(that._pictureSource.SAVEDPHOTOALBUM)
    },
    
    _getPhoto: function(source) {
        var that = this;
        // Retrieve image file location from specified source.
        navigator.camera.getPicture(function(){
            that._onPhotoURISuccess.apply(that,arguments);
        }, function(){
            cameraApp._onFail.apply(that,arguments);
        }, {
            quality: 50,
            destinationType: cameraApp._destinationType.FILE_URI,
            sourceType: source
        });
    },
    
    _onPhotoDataSuccess: function(imageData) {
        var smallImage = document.getElementById('smallImage');
        smallImage.style.display = 'block';
    
        // Show the captured photo.
        smallImage.src = "data:image/jpeg;base64," + imageData;
    },
    
    _onPhotoURISuccess: function(imageURI) {
        var smallImage = document.getElementById('smallImage');
        smallImage.style.display = 'block';
         
        // Show the captured photo.
        smallImage.src = imageURI;
    },
    
    _onFail: function(message) {
        alert(message);
    }
}

function captureApp() {
}

captureApp.prototype = {
    pictureSource:null,
    
    destinationType:null,
    
    run:function() {
        console.log ('running');
        var that = this;
        id("captureVideo").addEventListener("click", function() {
            that._captureVideo.apply(that, arguments);
        });
        id("captureAudio").addEventListener("click", function() {
            that._capureAudio.apply(that, arguments);
        });
        id("captureImage").addEventListener("click", function() {
            console.log ('hi');
            that._captureImage.apply(that, arguments);
        });
         id("playAudio").addEventListener("click", function() {
            that._playAudio.apply(that, arguments);
        });
    },
    
    _captureVideo:function() {
        var that = this;
        navigator.device.capture.captureVideo(function() {
            that._captureSuccess.apply(that, arguments);
        }, function() { 
            captureApp._captureError.apply(that, arguments);
        }, {limit:1});
    },
    
    _capureAudio:function() {
        var that = this;
        navigator.device.capture.captureAudio(function() {
            that._captureSuccess.apply(that, arguments);
        }, function() { 
            captureApp._captureError.apply(that, arguments);
        }, {limit:1});
    },
    
    _captureImage:function() {
        var that = this;
        navigator.device.capture.captureImage(function() {
            that._captureSuccess.apply(that, arguments);
        }, function() { 
            captureApp._captureError.apply(that, arguments);
        }, {limit:1});
    },
    
    _captureSuccess:function(capturedFiles) {
        var i,
        media = document.getElementById("media");
        media.innerHTML = "";
        for (i=0;i < capturedFiles.length;i+=1) {
            media.innerHTML+='<p>Capture taken! Its path is: ' + capturedFiles[i].fullPath + '</p>'
        }
         sessionStorage.setItem("myAudioPath", capturedFiles[0].fullPath);
        //$('#sampleImg').attr('src', capturedFiles[0].fullPath);
    },
    
    _captureError:function(error) {
        if (window.navigator.simulator === true) {
            alert(error);
        }
        else {
            var media = document.getElementById("media");
            media.innerHTML = "An error occured! Code:" + error.code;
        }
    },
    _playAudio:function() {
        var src=sessionStorage.getItem("myAudioPath"),
         media = document.getElementById("media");
        media.innerHTML = "play>>>" + src;
        
       var mediaContent = new Media(src, 
		  function() {
			 // that._onMediaSuccess.apply(that, arguments);
		  },
		  function() {
			  //that._onError.apply(that, arguments);
		  },
          function() {
              //that._onMediaStatusChanged.apply(that, arguments);
          }
        );
        mediaContent.play();

    }
}